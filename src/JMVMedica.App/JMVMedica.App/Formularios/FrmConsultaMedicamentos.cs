﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JMVMedica.App.Formularios
{
    public partial class FrmConsultaMedicamentos : DataFrm
    {
        public FrmConsultaMedicamentos()
        {
            InitializeComponent();
            dispensario = new Logic.Dispensario();
        }

        private void txtCriterio_TextChanged(object sender, EventArgs e)
        {
            Cargar(txtCriterio.Text);
        }

        protected override void Cargar(string criterio = "")
        {
            dgvResultado.DataSource = null;
            var resultado = dispensario.CargarMedicamentos()
                .Select(m => new
                {
                    Id = m.MedicamentoId,
                    m.Descripcion,
                    Tipo = m.TipoFarmaco.Descripcion,
                    Marca = m.Marca.Descripcion,
                    Ubicacion = m.Ubicacion.Descripcion,
                    m.Dosis
                }).Where(m =>
                    string.Format("{0}{1}{2}{3}{4}",
                    m.Descripcion,
                    m.Tipo, m.Marca,
                    m.Ubicacion,
                    m.Dosis).Contains(criterio));
            dgvResultado.DataSource = resultado.ToArray();
            dgvResultado.Refresh();
        }

    }
}
