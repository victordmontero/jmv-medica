﻿using JMVMedica.Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JMVMedica.App.Formularios
{
    public partial class FrmConsultaMarcas : Form
    {
        int _idActual = -1;
        Dispensario dispensario;
        public FrmConsultaMarcas()
        {
            InitializeComponent();
            dispensario = new Dispensario();
        }

        private void Cargar(string p)
        {
            var resultados = dispensario.CargarMarcas()
                .Select(x => new { Id = x.MarcaId, x.Descripcion })
                .Where(x=>x.Descripcion.Contains(p));
            dgvResultado.DataSource = resultados.ToArray();
            dgvResultado.Refresh();
        }

        private void txtCriterio_TextChanged(object sender, EventArgs e)
        {
            dgvResultado.DataSource = null;
            Cargar(txtCriterio.Text);
        }



    }
}
