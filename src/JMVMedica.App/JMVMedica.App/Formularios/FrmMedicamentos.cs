﻿using JMVMedica.Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JMVMedica.App.Formularios
{
    public partial class FrmMedicamentos : DataFrm
    {
        public FrmMedicamentos()
            : base()
        {
            InitializeComponent();
            dispensario = new Dispensario();
        }

        private void FrmMedicamentos_Load(object sender, EventArgs e)
        {
            Cargar();
            CargarCb();
        }

        private void CargarCb()
        {
            var tipoFarmacos = this.dispensario.CargarTipoFarmacos()
                .Select(x => new { x.TipoFarmacoId, x.Descripcion });

            var marcas = this.dispensario.CargarMarcas()
                .Select(x => new { x.MarcaId, x.Descripcion });

            var ubicaciones = this.dispensario.CargarUbicaciones()
                .Select(x => new { x.UbicacionId, x.Descripcion });

            cbMarca.DataSource = marcas.ToArray();
            cbMarca.ValueMember = "MarcaId";
            cbMarca.DisplayMember = "Descripcion";
            cbMarca.Refresh();

            cbTipoFarmaco.DataSource = tipoFarmacos.ToArray();
            cbTipoFarmaco.ValueMember = "TipoFarmacoId";
            cbTipoFarmaco.DisplayMember = "Descripcion";
            cbTipoFarmaco.Refresh();

            cbUbicacion.DataSource = ubicaciones.ToArray();
            cbUbicacion.ValueMember = "UbicacionId";
            cbUbicacion.DisplayMember = "Descripcion";
            cbUbicacion.Refresh();

        }

        protected override void Cargar(string criterio = "")
        {
            var medicamentos = dispensario.CargarMedicamentos()
                .Select(x => new
                {
                    Id = x.MedicamentoId,
                    x.Descripcion,
                    Marca = x.Marca.Descripcion,
                    Tipo = x.TipoFarmaco.Descripcion,
                    Ubicacion = x.Ubicacion.Descripcion,
                    x.Dosis
                });
            dgvMedicamentos.DataSource = medicamentos.ToArray();
            dgvMedicamentos.Refresh();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                if (_idActual < 0)
                {
                    dispensario.InsertarMedicamento(txtDescripcion.Text,
                            (int)cbTipoFarmaco.SelectedValue, (int)cbMarca.SelectedValue,
                            (int)cbUbicacion.SelectedValue, txtDosis.Text);
                }
                else
                    dispensario.EditarMedicamento(_idActual, txtDescripcion.Text,
                            (int)cbTipoFarmaco.SelectedValue, (int)cbMarca.SelectedValue,
                            (int)cbUbicacion.SelectedValue, txtDosis.Text);
                Cargar();
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "No se pudo guardar el medicamento");
            }
        }

        private void Limpiar()
        {
            txtDescripcion.Text = string.Empty;
            txtDosis.Text = string.Empty;
            cbUbicacion.SelectedIndex = -1;
            cbTipoFarmaco.SelectedIndex = -1;
            cbMarca.SelectedIndex = -1;
            _idActual = -1;
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (_idActual > -1)
                {
                    dispensario.BorrarMedicamento(_idActual);
                }
                else
                    MessageBox.Show(this, "Debe elegir un registro");

                Cargar();
                Limpiar();
            }
            catch (Exception)
            {
                MessageBox.Show(this, "Ocurrio un error al eliminar");
                throw;
            }
        }

        private void dgvMedicamentos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                var celdas = this.dgvMedicamentos.Rows[e.RowIndex].Cells;
                _idActual = (int)celdas["Id"].Value;
                txtDescripcion.Text = celdas["Descripcion"].Value.ToString();
                txtDosis.Text = celdas["Dosis"].Value.ToString();
                cbMarca.SelectedIndex = cbMarca.Items.IndexOf(celdas["Marca"].Value);
            }
        }
    }
}
