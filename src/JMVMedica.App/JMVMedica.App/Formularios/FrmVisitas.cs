﻿using JMVMedica.Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JMVMedica.App.Formularios
{
    public partial class FrmVisitas : DataFrm
    {
        public FrmVisitas()
        {
            InitializeComponent();
            dispensario = new Dispensario();
        }

        #region Metodos Custom
        protected override void Cargar(string criterio = "")
        {
            var resultados = dispensario.CargarVisitas()
                .Select(v => new
                {
                    Id = v.VisitaId,
                    Medico = v.Medico.Nombre,
                    Fecha = v.FechaVisita.ToString("dd/MM/yyyy")
                });
            dgvVisitas.DataSource = resultados.ToArray();
            dgvVisitas.Refresh();
        }

        private void CargarCb()
        {
            cbMedicamento.DataSource = dispensario.CargarMedicamentos()
                .Select(m => new
                {
                    Id = m.MedicamentoId,
                    m.Descripcion
                }).ToArray();
            cbMedicamento.DisplayMember = "Descripcion";
            cbMedicamento.ValueMember = "Id";
            cbMedicamento.Refresh();

            cbMedico.DataSource = dispensario.CargarMedicos()
                .Select(m => new
                {
                    Id = m.MedicoId,
                    Nombre = string.Format("{0}-{1}", m.Cedula, m.Nombre)
                }).ToArray();
            cbMedico.DisplayMember = "Nombre";
            cbMedico.ValueMember = "Id";
            cbMedico.Refresh();

            cbPaciente.DataSource = dispensario.CargarPacientes()
                .Select(p => new
                {
                    Id = p.PacienteId,
                    Carnet = string.Format("{0}-{1}", p.Carnet, p.Nombre)
                }).ToArray();
            cbPaciente.DisplayMember = "Carnet";
            cbPaciente.ValueMember = "Id";
            cbPaciente.Refresh();
        }

        void Limpiar()
        {
            _idActual = -1;
            txtRecomn.Text = string.Empty;
            txtSintomas.Text = string.Empty;
            cbMedicamento.SelectedIndex = -1;
            cbMedico.SelectedIndex = -1;
            cbPaciente.SelectedIndex = -1;
        }
        #endregion

        private void FrmVisitas_Load(object sender, EventArgs e)
        {
            CargarCb();
            Cargar();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void btnCrear_Click(object sender, EventArgs e)
        {
            try
            {
                dispensario.InsertarVisita((int)cbMedico.SelectedValue,
                    (int)cbPaciente.SelectedValue, (int)cbMedicamento.SelectedValue,
                    DateTime.Today, txtSintomas.Text, txtRecomn.Text);
                Cargar();
                Limpiar();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Hubo un error al guardar");
            }
        }
    }
}
