﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JMVMedica.App.Formularios
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void tipoDeFarmacosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmTipoFarmaco frmTipoFarmaco = new FrmTipoFarmaco();
            frmTipoFarmaco.MdiParent = this;
            frmTipoFarmaco.Show();
        }

        private void medicamentosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmMedicamentos frmMedicamento = new FrmMedicamentos();
            frmMedicamento.MdiParent = this;
            frmMedicamento.Show();
        }

        private void médicosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmMedicos frmMedico = new FrmMedicos();
            frmMedico.MdiParent = this;
            frmMedico.Show();
        }

        private void ubicacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmUbicaciones frmUbicaciones = new FrmUbicaciones();
            frmUbicaciones.MdiParent = this;
            frmUbicaciones.Show();
        }

        private void médicosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmConsultaMedicos frmConsultaMedico = new FrmConsultaMedicos();
            frmConsultaMedico.MdiParent = this;
            frmConsultaMedico.Show();
        }

        private void marcasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMarca frmMarca = new FrmMarca();
            frmMarca.MdiParent = this;
            frmMarca.Show();
        }

        private void pacientesToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmPacientes frmPacientes = new FrmPacientes();
            frmPacientes.MdiParent = this;
            frmPacientes.Show();
        }

        private void pacientesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmConsultaPacientes frmPacientes = new FrmConsultaPacientes();
            frmPacientes.MdiParent = this;
            frmPacientes.Show();
        }

        private void medicamentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmConsultaMedicamentos frmConsultaMedi = new FrmConsultaMedicamentos();
            frmConsultaMedi.MdiParent = this;
            frmConsultaMedi.Show();
        }

        private void visitasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmConsultaVisitas frmConVisitas = new FrmConsultaVisitas();
            frmConVisitas.MdiParent = this;
            frmConVisitas.Show();
        }

        private void nuevaVisitaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmVisitas frmVisitas = new FrmVisitas();
            frmVisitas.MdiParent = this;
            frmVisitas.Show();
        }

        private void FrmPrincipal_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
