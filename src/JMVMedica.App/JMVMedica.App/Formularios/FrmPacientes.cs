﻿using Helpers;
using JMVMedica.Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JMVMedica.App.Formularios
{
    public partial class FrmPacientes : DataFrm
    {
        public FrmPacientes()
        {
            InitializeComponent();
            dispensario = new Dispensario();
        }

        #region Metodos Custom
        protected override void Cargar(string criterio = "")
        {
            var pacientes = dispensario.CargarPacientes()
                .Select(x => new
                {
                    Id = x.PacienteId,
                    x.Nombre,
                    x.Cedula,
                    x.Carnet,
                    Tipo = Enum.GetName(typeof(TipoPaciente), x.TipoPaciente),
                });
            dgvPacientes.DataSource = pacientes.ToArray();
            dgvPacientes.Refresh();
        }

        private void CargarCb()
        {
            cbTipoPaciente.DataSource = Enum.GetValues(typeof(TipoPaciente));
            cbTipoPaciente.Refresh();
        }

        private void Limpiar()
        {
            _idActual = -1;
            txtNombre.Text = string.Empty;
            txtCedula.Text = string.Empty;
            txtCarnet.Text = string.Empty;
            cbTipoPaciente.SelectedIndex = -1;
        }
        #endregion

        private void FrmPacientes_Load(object sender, EventArgs e)
        {
            Cargar();
            CargarCb();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {

            try
            {
                if (PersonaHelper.ValidarCedula(txtCedula.Text))
                {
                    if (_idActual < 0)
                    {
                        dispensario.InsertarPaciente(txtNombre.Text, txtCedula.Text, txtCarnet.Text, (byte)cbTipoPaciente.SelectedValue);
                    }
                    else
                        dispensario.EditarPaciente(_idActual, txtNombre.Text, txtCedula.Text, txtCarnet.Text, (byte)cbTipoPaciente.SelectedValue);
                }
                else
                    MessageBox.Show(this, "Cedula invalida");

                Cargar();
                CargarCb();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error al insertar nuevo paciente");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (_idActual >= 0)
                    dispensario.BorrarPaciente(_idActual);

                Cargar();
                CargarCb();
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Error al eliminar paciente");
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void dgvPacientes_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                var celdas = dgvPacientes.Rows[e.RowIndex].Cells;
                _idActual = (int)celdas["Id"].Value;
                txtNombre.Text = celdas["Nombre"].Value.ToString();
                txtCedula.Text = celdas["Cedula"].Value.ToString();
                txtCarnet.Text = celdas["Carnet"].Value.ToString();
                cbTipoPaciente.SelectedIndex = cbTipoPaciente.Items.IndexOf(Enum.Parse(typeof(TipoPaciente), celdas["Tipo"].Value.ToString()));
            }
        }
    }
}
