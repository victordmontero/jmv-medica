﻿using JMVMedica.Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JMVMedica.App.Formularios
{
    public partial class FrmUbicaciones : DataFrm
    {
        public FrmUbicaciones()
        {
            InitializeComponent();
            dispensario = new Dispensario();
        }

        private void FrmUbicaciones_Load(object sender, EventArgs e)
        {
            Cargar();
        }

        protected override void Cargar(string criterio = "")
        {
            var ubicaciones = this.dispensario.CargarUbicaciones()
                .Select(x => new
                {
                    Id = x.UbicacionId,
                    x.Descripcion,
                    x.Estante,
                    x.Tramo,
                    x.Celda
                }).Where(u => criterio.Equals("") || u.Descripcion.StartsWith(criterio));
            dgvUbicaciones.DataSource = ubicaciones.ToArray();
            dgvUbicaciones.Refresh();
        }

        private void Limpiar()
        {
            _idActual = -1;
            txtDescripcion.Text = string.Empty;
            txtEstante.Text = string.Empty;
            numCelda.Value = 0;
            numTramo.Value = 0;
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                if (_idActual < 0)
                {
                    this.dispensario.InsertarUbicacion(txtDescripcion.Text,
                        txtEstante.Text, (int)numTramo.Value, (int)numCelda.Value);
                }
                else
                    this.dispensario.EditarUbicacion(_idActual, txtDescripcion.Text,
                        txtEstante.Text, (int)numTramo.Value, (int)numCelda.Value);

                Cargar();
                Limpiar();
            }
            catch (Exception)
            {
                MessageBox.Show(this, "Ocurrió un error al guardar");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (_idActual >= 0)
                {
                    this.dispensario.BorrarUbicacion(_idActual);
                }

                Cargar();
                Limpiar();
            }
            catch (Exception)
            {
                MessageBox.Show(this, "Ocurrió un error al eliminar");
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void dgvUbicaciones_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                var celdas = dgvUbicaciones.Rows[e.RowIndex].Cells;
                _idActual = (int)celdas["Id"].Value;
                txtDescripcion.Text = celdas["Descripcion"].Value.ToString();
                txtEstante.Text = celdas["Estante"].Value.ToString();
                numCelda.Value = (int)celdas["Celda"].Value;
                numTramo.Value = (int)celdas["Tramo"].Value;
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Cargar(txtDescripcion.Text);
        }
    }
}
