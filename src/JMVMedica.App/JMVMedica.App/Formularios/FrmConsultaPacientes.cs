﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JMVMedica.App.Formularios
{
    public partial class FrmConsultaPacientes : DataFrm
    {
        public FrmConsultaPacientes()
        {
            InitializeComponent();
            dispensario = new Logic.Dispensario();
        }

        private void txtCriterio_TextChanged(object sender, EventArgs e)
        {
            Cargar(txtCriterio.Text);
        }

        private void Cargar(string criterio)
        {
            dgvResultado.DataSource = null;
            var resultado = dispensario.CargarPacientes()
                .Select(x => new
                {
                    Id = x.PacienteId,
                    x.Nombre,
                    x.Cedula,
                    x.Carnet,
                    Tipo = Enum.GetName(typeof(TipoPaciente), x.TipoPaciente)
                }).Where(p => string.Format("{0}{1}{2}{3}", p.Nombre, p.Cedula, p.Carnet, p.Tipo).Contains(criterio));
            dgvResultado.DataSource = resultado.ToArray();
            dgvResultado.Refresh();
        }
    }
}
