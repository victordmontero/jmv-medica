﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JMVMedica.Logic;

namespace JMVMedica.App.Formularios
{
    public partial class FrmLogin : Form
    {
        public FrmLogin()
        {
            InitializeComponent();
        }

        private void chkClave_CheckedChanged(object sender, EventArgs e)
        {
            ActivarMostrarClave();
        }

        private void FrmLogin_Load(object sender, EventArgs e)
        {
            ActivarMostrarClave();
        }

        void ActivarMostrarClave()
        {
            txtClave.PasswordChar = chkClave.Checked ? '\0' : '*';
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            try
            {
                FrmPrincipal frmP = new FrmPrincipal();
                if (new Seguridad(txtUsuario.Text, txtClave.Text).Autenticar())
                {
                    frmP.Show();
                    this.Hide();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, "Usuario o clave incorrecto");
            }
        }
    }
}
