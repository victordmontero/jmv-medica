﻿using Helpers;
using JMVMedica.Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JMVMedica.App.Formularios
{
    public partial class FrmMedicos : DataFrm
    {
        public FrmMedicos()
        {
            InitializeComponent();
            dispensario = new Dispensario();
        }

        #region Métodos Custom
        /// <summary>
        /// Método para validar la cédula
        /// </summary>
        /// <returns></returns>
        public bool validarCedulaMedico()
        {

            try
            {
                String cedulaMedico = txtCedula.Text;
                int cedula = int.Parse(cedulaMedico);
            }
            catch (Exception ex)
            {

                MessageBox.Show("Se a producido un error");
                MessageBox.Show(ex.ToString());
                return false;

            }

            return true;
        }

        private void Limpiar()
        {
            _idActual = -1;
            txtNombreDoctor.Text = string.Empty;
            txtEspecialidad.Text = string.Empty;
            txtCedula.Text = string.Empty;
            comboTanda.SelectedIndex = -1;
        }

        protected override void Cargar(string criterio = "")
        {
            var medicos = dispensario.CargarMedicos()
                .Select(x => new
                {
                    Id = x.MedicoId,
                    Nombre = x.Nombre,
                    Cedula = x.Cedula,
                    Tanda = Enum.GetName(typeof(TandaLabor), x.TandaLabor),
                    Especialidad = x.Especialidad
                });
            dgvMedicos.DataSource = medicos.ToArray();
            dgvMedicos.Refresh();
        }

        private void CargarCb()
        {
            this.comboTanda.DataSource = Enum.GetValues(typeof(TandaLabor));
        }
        #endregion

        private void FrmMedicos_Load(object sender, EventArgs e)
        {
            Cargar();
            CargarCb();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                if (PersonaHelper.ValidarCedula(txtCedula.Text))
                {
                    if (_idActual < 0)
                    {
                        this.dispensario.InsertarMedico(txtNombreDoctor.Text,
                            txtCedula.Text, (byte)comboTanda.SelectedValue, txtEspecialidad.Text);
                    }
                    else
                        this.dispensario.EditarMedico(_idActual, txtNombreDoctor.Text,
                            txtCedula.Text, (byte)comboTanda.SelectedValue, txtEspecialidad.Text);
                }
                else
                    MessageBox.Show(this, "Cedula invalida");

                Cargar();
                Limpiar();
            }
            catch (Exception)
            {
                MessageBox.Show(this, "Ocurrió un error al guardar");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (_idActual >= 0)
                {
                    this.dispensario.BorrarMedico(_idActual);
                    Cargar();
                    Limpiar();
                }
                else
                    MessageBox.Show(this, "Debe seleccionar un elemento de la lista");
            }
            catch (Exception)
            {
                MessageBox.Show(this, "Ocurrió un error al eliminar");
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void dgvMedicos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                var celdas = this.dgvMedicos.Rows[e.RowIndex].Cells;
                _idActual = (int)celdas["Id"].Value;
                txtNombreDoctor.Text = celdas["Nombre"].Value.ToString();
                txtCedula.Text = celdas["Cedula"].Value.ToString();
                txtEspecialidad.Text = celdas["Especialidad"].Value.ToString();
                comboTanda.SelectedIndex = comboTanda.Items.IndexOf(Enum.Parse(typeof(TandaLabor), celdas["Tanda"].Value.ToString()));
            }

        }


    }
}
