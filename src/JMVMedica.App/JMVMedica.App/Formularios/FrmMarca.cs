﻿using JMVMedica.Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JMVMedica.App.Formularios
{
    public partial class FrmMarca : DataFrm
    {
        public FrmMarca()
            : base()
        {
            InitializeComponent();
            dispensario = new Dispensario();
        }

        #region Metodos Custom
        protected override void Cargar(string criterio = "")
        {
            var marcas = dispensario.CargarMarcas()
                .Select(x => new { Id = x.MarcaId, x.Descripcion })
                .Where(m => criterio.Equals("") || m.Descripcion.StartsWith(criterio));
            dgvMarcas.DataSource = marcas.ToArray();
            dgvMarcas.Refresh();
        }

        void Limpiar()
        {
            _idActual = -1;
            txtDescripcion.Text = string.Empty;
        }
        #endregion

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            try
            {
                if (_idActual >= 0)
                {
                    dispensario.EditarMarca(_idActual, txtDescripcion.Text);
                }
                else
                    dispensario.InsertarMarca(txtDescripcion.Text);

                Limpiar();
                Cargar();
            }
            catch (Exception)
            {
                MessageBox.Show(this, "Ocurrio un error al agregar");
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                if (_idActual >= 0)
                {
                    dispensario.BorrarMarca(_idActual);
                }

                Limpiar();
                Cargar();
            }
            catch (Exception)
            {
                MessageBox.Show(this, "Ocurrio un error al eliminar");
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void FrmMarca_Load(object sender, EventArgs e)
        {
            Cargar();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            Cargar(txtDescripcion.Text);
        }

        private void dgvMarcas_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex >= 0)
            {
                var celdas = dgvMarcas.Rows[e.RowIndex].Cells;
                _idActual = (int)celdas["Id"].Value;
                txtDescripcion.Text = celdas["Descripcion"].Value.ToString(); 
            }
        }
    }
}
