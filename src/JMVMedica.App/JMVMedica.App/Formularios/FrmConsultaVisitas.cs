﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JMVMedica.App.Formularios
{
    public partial class FrmConsultaVisitas : DataFrm
    {
        public FrmConsultaVisitas()
        {
            InitializeComponent();
            dispensario = new Logic.Dispensario();
        }

        private void txtCriterio_TextChanged(object sender, EventArgs e)
        {
            Cargar(txtCriterio.Text);
        }

        protected override void Cargar(string criterio = "")
        {
            dgvResultado.DataSource = null;
            var resultado = dispensario.CargarVisitas()
                .Select(v => new
                {
                    Id = v.VisitaId,
                    Medico = v.Medico.Nombre,
                    Paciente = v.Paciente.Nombre,
                    Medicamento = v.Medicamento.Descripcion,
                    Fecha = v.FechaVisita.ToString("dd/MM/yyyy"),
                    v.Sintomas,
                    v.Recomendaciones
                }).Where(v => criterio.Equals("") ||
                    string.Format("{0}{1}{2}{3}{4}{5}",
                    v.Medico, v.Paciente, v.Medicamento, v.Sintomas,
                    v.Recomendaciones, v.Fecha).Contains(criterio));
            dgvResultado.DataSource = resultado.ToArray();
            dgvResultado.Refresh();
        }
    }
}
