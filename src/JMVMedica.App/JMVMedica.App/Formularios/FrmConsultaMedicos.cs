﻿using JMVMedica.Logic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace JMVMedica.App.Formularios
{
    public partial class FrmConsultaMedicos : Form
    {
        Dispensario dispensario;
        public FrmConsultaMedicos()
        {
            InitializeComponent();
            dispensario = new Dispensario();
        }

        public void Cargar(string criterio)
        {
            dgvResultado.DataSource = null;
            var resultados = dispensario.CargarMedicos()
                .Select(x => new
                {
                    x.Nombre,
                    x.Cedula,
                    x.Especialidad,
                    Tanda = Enum.GetName(typeof(TandaLabor), x.TandaLabor)
                }).Where(c => (string.Format("{0}{1}{2}{3}", c.Nombre, c.Cedula, c.Especialidad, c.Tanda)).Replace(' ', '\0').Contains(criterio));
            dgvResultado.DataSource = resultados.ToArray();
            dgvResultado.Refresh();
        }

        private void FrmConsultaMedicos_Load(object sender, EventArgs e)
        {

        }

        private void txtCriterio_TextChanged(object sender, EventArgs e)
        {
            Cargar(txtCriterio.Text);
        }
    }
}
