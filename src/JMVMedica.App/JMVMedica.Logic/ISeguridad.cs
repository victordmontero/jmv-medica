﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMVMedica.Logic
{
    public interface ISeguridad
    {
        bool Autenticar();
        string Encriptar(string passwd);
        void InsertarUsuario(string usuario, string passwd);
        void EditarUsuario(int usuarioId, string nombre, string passwd);
        void Borrar(string usuarioId);
        IEnumerable<dynamic> CargarUsuarios();
    }
}
