﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Text;
using System.Data.SqlClient;
using System.Configuration;

namespace JMVMedica.Logic
{
    public class Seguridad : ISeguridad
    {
        private string usr;
        private string passwd;
        private SqlConnection ocon;

        public string Usr { get { return usr; } set { usr = value; } }
        public string Passwd { get { return passwd; } set { passwd = value; } }

        public Seguridad(string usr = "", string passwd = "")
        {
            ocon = new SqlConnection(ConfigurationManager.ConnectionStrings["JMVMedica"].ToString());
            this.usr = usr;
            this.passwd = passwd;
        }

        public bool Autenticar()
        {
            try
            {
                var usrs = CargarUsuarios().ToList();
                var usuario = usrs.Single(x => x.Usuario.Equals(usr));

                return usuario.Clave.Equals(Encriptar(passwd + usuario.Sal));
            }
            catch (Exception)
            {
                return false;
            }

        }

        public string Encriptar(string passwd)
        {
            var strBuilder = new StringBuilder();

            byte[] passwdBytes = ASCIIEncoding.ASCII.GetBytes(passwd);
            byte[] result = null;
            using (var algo = new MD5Cng())
            {
                result = algo.ComputeHash(passwdBytes);
            }

            foreach (var caracter in result)
            {
                strBuilder.Append(caracter.ToString("x2"));
            }

            return strBuilder.ToString();
        }

        public void InsertarUsuario(string usuario, string passwd)
        {
            try
            {
                ocon.Open();
                using (SqlCommand cmd = new SqlCommand("CrearUsuario", ocon))
                {
                    cmd.Parameters.AddRange(new SqlParameter[] {
                        new SqlParameter("@Usuario",usuario),
                        new SqlParameter("@Clave",Encriptar(passwd+DateTime.Today.Ticks)),
                        new SqlParameter("@Sal",DateTime.Today.Ticks)
                    });

                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        throw new Exception("Error al Guardar Usuario");
                    }
                }
                ocon.Close();
            }
            finally
            {
                ocon.Close();
            }
        }

        public void EditarUsuario(int usuarioId, string nombre, string passwd)
        {
            try
            {
                ocon.Open();
                using (var cmd = new SqlCommand("EditarUsuario", ocon))
                {
                    cmd.Parameters.AddRange(new SqlParameter[] {
                        new SqlParameter("@UsuarioId",usuarioId),
                        new SqlParameter("@Usuario",nombre),
                        new SqlParameter("@Clave",passwd)
                    });

                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        throw new Exception("Hubo un error al editar");
                    }
                }
                ocon.Close();
            }
            finally
            {
                ocon.Close();
            }
        }

        public void Borrar(string usuarioId)
        {
            try
            {
                ocon.Open();
                using (var cmd = new SqlCommand("UPDATE Usuarios SET Estado = " +
                    "0 WHERE UsuarioId = " + usuarioId, ocon))
                {
                    if (cmd.ExecuteNonQuery() != 0)
                    {
                        throw new Exception("Hubo un error al editar");
                    }
                }
                ocon.Close();
            }
            finally
            {
                ocon.Close();
            }
        }

        public IEnumerable<dynamic> CargarUsuarios()
        {
            List<dynamic> results = new List<dynamic>();

            try
            {
                ocon.Open();
                using (SqlCommand cmd = new SqlCommand("ObtenerUsuarios", ocon))
                {
                    var reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        results.Add(new
                        {
                            UsuarioId = reader["UsuarioId"].ToString(),
                            Usuario = reader["Usuario"].ToString(),
                            Clave = reader["Clave"].ToString(),
                            Sal = reader["Sal"].ToString()
                        });
                    }
                }
                ocon.Close();
                return results;
            }
            finally
            {
                ocon.Close();
            }
        }
    }
}
