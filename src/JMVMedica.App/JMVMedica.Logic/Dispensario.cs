﻿using JMVMedica.DataAccess.Contexto;
using JMVMedica.DataAccess.Modelos;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JMVMedica.Logic
{
    public class Dispensario : IDisposable
    {
        private JMVContexto _contexto;
        public Dispensario()
        {
            _contexto = new JMVContexto();
        }

        #region TipoFarmaco
        public void InsertarTipoFarmaco(string descripcion, bool estado = true)
        {
            this._contexto.TipoFarmacos.Add(new TipoFarmaco()
            {
                Descripcion = descripcion,
                Estado = estado
            });
            this._contexto.SaveChanges();
        }

        public void EditarTipoFarmaco(int tipoFarmacoId, string descripcion, bool estado = true)
        {
            var tipoFarmaco = this._contexto.TipoFarmacos.Find(tipoFarmacoId);
            tipoFarmaco.Descripcion = descripcion;
            tipoFarmaco.Estado = estado;

            this._contexto.SaveChanges();
        }

        public void BorrarTipoFarmaco(int tipoFarmaco)
        {
            var tf = this._contexto.TipoFarmacos.Find(tipoFarmaco);
            tf.Estado = false;
            this._contexto.SaveChanges();
        }

        public IEnumerable<dynamic> CargarTipoFarmacos()
        {
            return this._contexto.TipoFarmacos.Where(x => x.Estado).ToArray();

        }
        #endregion

        #region Medicamentos
        public void InsertarMedicamento(string descripcion, int tipoFarmacoId, int marcaId, int ubicacionId, string dosis, bool estado = true)
        {
            this._contexto.Medicamentos.Add(new Medicamento()
            {
                Descripcion = descripcion,
                TipoFarmacoId = tipoFarmacoId,
                MarcaId = marcaId,
                UbicacionId = ubicacionId,
                Dosis = dosis,
                Estado = estado
            });
            this._contexto.SaveChanges();
        }

        public void EditarMedicamento(int medicamentoId, string descripcion, int tipoFarmacoId, int marcaId, int ubicacionId, string dosis, bool estado = true)
        {
            var medicamento = this._contexto.Medicamentos.Find(medicamentoId);
            medicamento.Descripcion = descripcion;
            medicamento.TipoFarmacoId = tipoFarmacoId;
            medicamento.MarcaId = marcaId;
            medicamento.UbicacionId = ubicacionId;
            medicamento.Dosis = dosis;
            medicamento.Estado = estado;

            this._contexto.SaveChanges();
        }

        public void BorrarMedicamento(int medicamentoId)
        {
            var medicamento = this._contexto.Medicamentos.Find(medicamentoId);
            medicamento.Estado = false;

            this._contexto.SaveChanges();
        }

        public IEnumerable<dynamic> CargarMedicamentos()
        {
            return this._contexto.Medicamentos.Include(x => x.Marca)
                .Include(x => x.Ubicacion)
                .Include(x => x.TipoFarmaco)
                .Where(x => x.Estado).ToArray();
        }
        #endregion

        #region Medicos
        public void InsertarMedico(string nombre, string cedula, byte tanda, string especialidad, bool estado = true)
        {
            this._contexto.Medicos.Add(new Medico()
            {
                Nombre = nombre,
                Cedula = cedula,
                TandaLabor = tanda,
                Especialidad = especialidad,
                Estado = estado
            });
            this._contexto.SaveChanges();
        }

        public void EditarMedico(int medicoId, string nombre, string cedula, byte tanda, string especialidad, bool estado = true)
        {
            var medico = this._contexto.Medicos.Find(medicoId);
            medico.Nombre = nombre;
            medico.Cedula = cedula;
            medico.TandaLabor = tanda;
            medico.Especialidad = especialidad;
            medico.Estado = estado;

            this._contexto.SaveChanges();
        }

        public void BorrarMedico(int medicoId)
        {
            var medico = this._contexto.Medicos.Find(medicoId);
            medico.Estado = false;

            this._contexto.SaveChanges();
        }

        public IEnumerable<dynamic> CargarMedicos()
        {
            return this._contexto.Medicos.Where(x => x.Estado).ToArray();
        }
        #endregion

        #region Ubicaciones
        public void InsertarUbicacion(string descripcion, string estante, int tramo, int celda, bool estado = true)
        {
            this._contexto.Ubicaciones.Add(new Ubicacion()
            {
                Descripcion = descripcion,
                Estante = estante,
                Tramo = tramo,
                Celda = celda,
                Estado = estado
            });
            this._contexto.SaveChanges();
        }

        public void EditarUbicacion(int ubicacionId, string descripcion, string estante, int tramo, int celda, bool estado = true)
        {
            var ubicacion = this._contexto.Ubicaciones.Find(ubicacionId);
            ubicacion.Descripcion = descripcion;
            ubicacion.Estante = estante;
            ubicacion.Tramo = tramo;
            ubicacion.Celda = celda;
            ubicacion.Estado = estado;

            this._contexto.SaveChanges();
        }

        public void BorrarUbicacion(int ubicacionId)
        {
            var ubicacion = this._contexto.Ubicaciones.Find(ubicacionId);

            ubicacion.Estado = false;

            this._contexto.SaveChanges();
        }

        public IEnumerable<dynamic> CargarUbicaciones()
        {
            return this._contexto.Ubicaciones.Where(x => x.Estado).ToArray();
        }
        #endregion

        #region Marcas
        public void InsertarMarca(string descripcion, bool estado = true)
        {
            this._contexto.Marcas.Add(new Marca()
            {
                Descripcion = descripcion,
                Estado = estado
            });
            this._contexto.SaveChanges();
        }

        public void EditarMarca(int marcaId, string descripcion, bool estado = true)
        {
            var marca = this._contexto.Marcas.Find(marcaId);
            marca.Descripcion = descripcion;
            marca.Estado = estado;
            this._contexto.SaveChanges();
        }

        public void BorrarMarca(int marcaId)
        {
            var marca = this._contexto.Marcas.Find(marcaId);
            marca.Estado = false;
            this._contexto.SaveChanges();
        }

        public IEnumerable<dynamic> CargarMarcas()
        {
            return this._contexto.Marcas.Where(x => x.Estado).ToArray();
        }
        #endregion

        #region Paciente
        public void InsertarPaciente(string nombre, string cedula, string carnet, byte tipo, bool estado = true)
        {
            this._contexto.Pacientes.Add(new Paciente()
            {
                Nombre = nombre,
                Cedula = cedula,
                Carnet = carnet,
                TipoPaciente = tipo,
                Estado = estado
            });

            this._contexto.SaveChanges();
        }

        public void EditarPaciente(int pacienteId, string nombre, string cedula, string carnet, byte tipo, bool estado = true)
        {
            var paciente = this._contexto.Pacientes.Find(pacienteId);
            paciente.Nombre = nombre;
            paciente.Cedula = cedula;
            paciente.Carnet = carnet;
            paciente.TipoPaciente = tipo;
            paciente.Estado = estado;
            this._contexto.SaveChanges();
        }

        public void BorrarPaciente(int pacienteId)
        {
            var paciente = this._contexto.Pacientes.Find(pacienteId);
            paciente.Estado = false;
            this._contexto.SaveChanges();
        }

        public IEnumerable<dynamic> CargarPacientes()
        {
            return this._contexto.Pacientes.Where(p => p.Estado).ToArray();
        }
        #endregion

        #region Visitas
        public void InsertarVisita(int medicoId,
            int pacienteId,
            int medicamentoId,
            DateTime fechaVisita,
            string sintomas,
            string recomendaciones, bool estado = true)
        {
            _contexto.Visitas.Add(new Visita()
            {
                MedicoId = medicoId,
                PacienteId = pacienteId,
                MedicamentoId = medicamentoId,
                FechaVisita = fechaVisita,
                Sintomas = sintomas,
                Recomendaciones = recomendaciones,
                Estado = estado
            });
            _contexto.SaveChanges();
        }

        public IEnumerable<dynamic> CargarVisitas()
        {
            return _contexto.Visitas.Include(m => m.Medico)
                .Include(m => m.Medicamento)
                .Include(p => p.Paciente)
                .Where(v => v.Estado).ToArray();
        }
        #endregion

        public void Dispose()
        {
            this._contexto.Dispose();
        }
    }
}
